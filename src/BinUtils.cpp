#include "BinUtils.h"

#include <algorithm>

BinUtils::BinUtils(Log* log_ref)
    : _log_ref(log_ref)
{}

void BinUtils::setLog(Log* log_ref){
    _log_ref = log_ref;
}

uint16_t BinUtils::revert16(uint16_t value){
    uint8_t lo = getLow16(value);
    uint8_t hi = getHi16(value);
    std::swap(lo, hi);
    return make16(lo, hi);
}

uint16_t BinUtils::make16(uint8_t lo, uint8_t hi){
    return (hi << 8) | lo;
}

uint32_t BinUtils::make32(uint16_t lo, uint16_t hi){
    return (hi << 16) | lo;
}

uint32_t BinUtils::make32(uint8_t lo_lo, uint8_t lo_hi, uint8_t hi_lo, uint8_t hi_hi){
    return (hi_hi << 24) | (hi_lo << 16) | (lo_hi << 8) | lo_lo; 
}

uint64_t BinUtils::make64(uint32_t lo, uint32_t hi){
    return 0;
}

uint32_t BinUtils::revert32(uint32_t value){
    uint16_t lo = getLow32(value);
    uint16_t hi = getHi32(value);
    std::swap(lo, hi);
    return make32(lo, hi);
}

uint32_t BinUtils::getHi64(uint64_t value){
    return value >> 32;
}

uint32_t BinUtils::getLo64(uint64_t value){
    return value;
}


uint64_t BinUtils::revert64(uint64_t value){
    uint32_t lo = getLo64(value);
    uint32_t hi = getHi64(value);
    std::swap(lo, hi);
    return make64(lo, hi);
}

uint8_t BinUtils::getLow16(uint16_t value){
    return uint16_t(value);
}

uint8_t BinUtils::getHi16(uint16_t value){
    return (value >> 8);
}

uint16_t BinUtils::getLow32(uint32_t value){
    return uint16_t(value);
}

uint16_t BinUtils::getHi32(uint32_t value){
    return (value >> 16);
}
