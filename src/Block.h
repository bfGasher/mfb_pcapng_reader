#pragma once 

#include <fstream>
#include <iostream>
#include <cstdint>


#include "Option.h"
#include "BinUtils.h"
#include "Log.h"

class Block {
protected:
    uint32_t _type;    // тип блока
    uint32_t _length;  // размер блока в байта полностью
    // ссылка на логгер. для того 
    // чтобы логировать состояние объекта
    Log* _log_ref; 
    BinUtils* _bin_utils_ref; // утилиты для работы с байтами
    OptionsVec _options; // опции блока
    bool _end_opt_readed; // конец опций прочитан
    // ф-ии для работы с интами для переворачивания байт для 
    // получения BE представлений целочисленных чисел
    void revertWord(uint16_t& value);
    void revertDoubleWord(uint32_t& value);
    void revertQuadWord(uint64_t& value);

    virtual std::string optCodeToStr(uint16_t code);   
    virtual bool extractOption(uint16_t code, uint16_t len, std::ifstream& stream, bool big_end);
    void parseOptions(std::ifstream& stream, bool big_end);

public:
    // передаём указатель на логгер для его использования при выдачи сообщений 
    // на экран. По умолчанию передаём нулл чтобы можно было вызывать иногда без явного 
    // вызова Block(nullptr)

    // тоесть в данном контексте вызовы Block() и Block(nullptr) == эквивалентны
    Block(Log* log_ref,  BinUtils* bin_utils_ref);
    Block(uint32_t type, uint32_t length, Log* log_ref, BinUtils* bin_utils_ref);

    virtual ~Block(); // деструктор -- виртуальный обязательно так как есть наследники с виртуальными переопределяемыми методами.
    virtual void parse(std::ifstream& stream, bool big_end); // чтение из потока в соответствии с флагом BE\LE
    virtual void output(std::ostream& stream); // выдача в поток сведений о потоке (вывод на экран данных если поддерживается метод)
    virtual void outputOptions(std::ostream& stream);
    virtual std::string toHexString(); // попытка получить данные блока в виде hex строки

    uint32_t& type(); // получить тип блока
    uint32_t& length(); // получить размер блока в байтах
};

/*
задумка -- прочитать 
весь отведённый под блок буфер и
парсить данные уже из него.

Хранить в Block сам буфер вместе с полями
для того чтобы потом допарсить без 
ридера. Например те же опции или 
данные по сетевым пакетам к примеру.

Но это совсем на будущее.
*/

// Заголовочный блока
class HeaderBlock: public Block {
protected:
    uint32_t _order_magic;    // порядок байт 
    uint16_t _minor_version; // малая версия 
    uint16_t _major_version; // главная версия
    uint64_t _section_length; // размер секции
    // options
    bool extractOption(uint16_t code, uint16_t len, std::ifstream& stream, bool big_end) override;
public:
    HeaderBlock(Log* log_ref, BinUtils* bin_utils_ref);
    void parse(std::ifstream& stream, bool big_end) override;
    void output(std::ostream& stream) override;
    uint32_t& order_magic(); // порядок байт
    uint16_t& minor_version();  
    uint16_t& major_version();
    uint64_t& section_length();
};

// Блок интерфейсв
class InterfaceDescriptionBlock: public Block{
protected:
    uint32_t _snap_len;  // размер какого снимка...
    uint16_t _link_type; // тип ссылки (проще по вики ориентироваться...)
    uint16_t _reserved; // не используется нигде
    std::string optCodeToStr(uint16_t code) override;
    //void parseOptions(std::ifstream& stream, bool big_end) override;
    bool extractOption(uint16_t code, uint16_t len, std::ifstream& stream, bool big_end) override;
public:
    InterfaceDescriptionBlock(Log* log_ref, BinUtils* bin_utils_ref); // указатель на логгер
    void parse(std::ifstream& stream, bool big_end) override;
    void output(std::ostream& stream) override;

    uint16_t& linkType();
}; 

// "Улучшенный пакетный блок" (с) гугл транслейт
class EnhancedPacketBlock: public Block {
protected:
    uint32_t _interface_id; // идентификатор какого то интерФейса
    uint32_t _timestamp_hi; // высокий юникс тайм
    uint32_t _timestamp_low; // низкий юникс тайм
    uint32_t _captured_packet_len; // размер снимка сетевого пакета данных
    uint32_t _original_packet_len; // размер оригинальных данныз пакета (???) пальцы заплятаюся.юю
    uint8_t* _packet_data;
    bool extractOption(uint16_t code, uint16_t len, std::ifstream& stream, bool big_end) override;
public:
    EnhancedPacketBlock(Log* log_ref, BinUtils* bin_utils_ref);
    ~EnhancedPacketBlock();
    void parse(std::ifstream& stream, bool big_end) override;
    void output(std::ostream& stream) override;

    uint32_t& interface_id();
    uint32_t& timestamp_hi();
    uint32_t& timestamp_low();
    uint32_t& captured_packet_len();
    uint32_t& original_packet_len();
};

// 
class SimplePacketBlock: public Block {
protected:
    uint32_t _original_packet_length;
public:
    SimplePacketBlock(Log* log_ref, BinUtils* bin_utils_ref);
    void parse(std::ifstream& stream, bool big_end) override;
};