#include "Log.h"

#include <iostream>
#include <fstream>
#include <list>
#include <string>

Log::Log(const std::string& name): _name(name), _buffer(), _print_to_console(false){
    if (_name == ""){
        // еси не задали имя логгеру то задём ему дефолтное 
        _name = "log";
    }
}

Log& Log::log(const std::string& message){

    // если выставили флажок -- печать в консоль то паралельно записи в буффер пищем и в консоль ешё
    if (_print_to_console){
        std::cout << "[log: "<< _name << "] " << message << std::endl;    
    }    
    _buffer.push_back("[log: "+ _name + "] " + message);
    return *this;
}

void Log::save(){
    // сохранить в файл всё что успел вобрать в себя логгер
    std::ofstream stream(_name + ".log", std::ofstream::app); // открываем файл на добавление текста
    for (auto i = _buffer.begin(); i != _buffer.end(); ++i){ // проходимся по каждой строке в _buffer 
        stream << *i << std::endl; // эквивалентно почти // stream << buffer[i] << endl // если i == int
    }
    stream.close(); // закрываем поток после всех операций записи в файл
}

void Log::setPrintToConsole(bool flag){
    _print_to_console = flag;
}