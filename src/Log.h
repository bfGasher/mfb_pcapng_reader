#pragma once

#include <string>
#include <list>

// Логгер - пишет строки в буфер и потом по запросу
// сохраняет в одноименный файл
class Log {
private:
    std::string _name; // имя логгера - для идентификации и именования файла
    std::list<std::string> _buffer; // список со строками которые мы получаем вызывя метод log(message)
    bool _print_to_console; // флаг -- писать ли логгеру в консоль
public:
    Log(const std::string& name);
    Log& log(const std::string& message);
    void setPrintToConsole(bool flag);
    void save(); // сохранить лог в файл
};