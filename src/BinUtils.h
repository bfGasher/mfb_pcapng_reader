#pragma once 

#include <cstdint>

#include "Log.h"


// утилитный класс для работы с преообразованием 
// целочисленных байтовых структур данных

// операции переворачивания конструирования и деконструирования
// целочисленных переменных на составляющие (на байтики и битики)
class BinUtils {
private:
    Log* _log_ref;
public:
    BinUtils(Log* log_ref);

    void setLog(Log* log_ref);

    uint16_t make16(uint8_t lo, uint8_t hi);
    uint32_t make32(uint8_t lo_lo, uint8_t lo_hi, uint8_t hi_lo, uint8_t hi_hi);
    uint32_t make32(uint16_t lo, uint16_t hi);
    uint64_t make64(uint32_t lo, uint32_t hi);

    uint8_t getLow16(uint16_t value);
    uint8_t getHi16(uint16_t value);
    uint16_t getLow32(uint32_t value);
    uint16_t getHi32(uint32_t value);
    uint32_t getHi64(uint64_t value);
    uint32_t getLo64(uint64_t value);

    uint16_t revert16(uint16_t value);
    uint32_t revert32(uint32_t value);
    uint64_t revert64(uint64_t value);
};