#include "Block.h"

#include <cstring>
#include <sstream>
#include <string>

#include "BlockType.h"

using namespace std;

/* Block */

// конструкктор без параметров
Block::Block(Log* log_ref, BinUtils* bin_utils_ref): Block(0, 0, log_ref, bin_utils_ref) {}

// конструкктор с параметрами для удобной инициализации
Block::Block(uint32_t type, uint32_t length, Log* log_ref, BinUtils* bin_utils_ref) :
    _type(type), // задаём тип блока
    _length(length), // задаём размер блока
    _log_ref(log_ref),
    _bin_utils_ref(bin_utils_ref),
    _end_opt_readed(false)
{}

Block::~Block() {
    // не владеем логером. удаляется в деструкторе App
    // зануляем чисто ради безопасности.
    _log_ref = nullptr;
    if (_options.size() > 0){
        for (int64_t i = 0; i < _options.size(); i++ ){
            if (_options[i] != nullptr){
                delete _options[i];
            }
        }
        // делаем очистку опций
    }
    _options.clear();
}

// вернуть тип блока 
uint32_t& Block::type(){
    return _type;
}

// вернуть размер блока в байтах
uint32_t& Block::length(){
    return _length;
}

std::string Block::optCodeToStr(uint16_t code){
    switch(code){
        case 0:
            return "opt_endofopt";
        case 1:
            return "opt_comment";
        default:
            return "<unknown>";
    }
}

bool Block::extractOption(uint16_t code, uint16_t len, std::ifstream& stream, bool big_end){
    // todo: 
    Option* opt = nullptr;
    switch(code){
        case 0:
            _end_opt_readed = true;
            break;
        case 1:
            if (len > 0 ){
                uint8_t* buffer = new uint8_t[len];
                Option* opt = new Option(code, len);    
                for (int32_t i = 0; i < len; i++){
                    opt->value().push_back(buffer[i]);
                }
                _options.push_back(opt);        
                delete[] buffer;
            }
            
            break;
        default:
            return false;
    }

    if (len != 0){
        uint32_t padding_len = len;
        uint8_t* buff = new uint8_t[padding_len];
        stream.read((char*)buff, padding_len);
        delete buff;          
    }    

    return true;
}

void Block::parseOptions(std::ifstream& stream, bool big_end){
    while (!_end_opt_readed){
        uint32_t opt_header = 0;
        stream.read((char*)&opt_header, sizeof(opt_header));
        uint16_t opt_code = _bin_utils_ref->getLow32(opt_header);
        uint16_t opt_len  = _bin_utils_ref->getHi32(opt_header);
        _log_ref->log("opt_code = " + std::to_string(opt_code));
        _log_ref->log("opt_len = " + std::to_string(opt_len));
        if (!extractOption(opt_code, opt_len, stream, big_end)){
            _log_ref->log("opt_endofopt reached. no more option's in this block");
            _log_ref->log("make step back in stream.");
            int64_t pos = stream.tellg(); // берём текущую позицию
            // колдунство. прочитали тип и шагнули назад на 4 байта     
            stream.seekg(pos - sizeof(opt_header), std::ios::beg); // шагаем взад           
            break;
        }
    }
} 

void Block::outputOptions(std::ostream& stream){
    _log_ref->log("options: count = " + std::to_string(_options.size()));
    for (Option* o: _options){
        std::string log_str ="-- " + optCodeToStr(o->code());
        if (o->value().size() > 0){
            log_str += " ";
            for (int32_t i = 0; i < o->value().size(); i++){
                log_str += o->value()[i];
            }
        }
        _log_ref->log(log_str);    
    }     
}

// распарсить 
void Block::parse(std::ifstream& stream, bool big_end){
    stream.read((char*)&_type, sizeof(_type)); // парсим тип блока
    stream.read((char*)&_length, sizeof(_length)); // парсим размер блока
    if (big_end){
        revertDoubleWord(_type); // fix: заменить на _bin_utils->revert32(_type);
        revertDoubleWord(_length); // fix: заменить на _bin_utils->revert32(_length);
    }
}

void Block::output(std::ostream& stream){
    stream << "type = " << blockTypeToStr(_type) << " ("<<std::hex << _type << std::dec << ")"<< std::endl
        << "length = " << _length << " (" << std::hex << _length << std::dec << ")" << std::endl;   
}


// переворот байт в BE / LE  для 2 байтового целого
void Block::revertWord(uint16_t& value){
    // debug
    // _log_ref->log("revertWord()");
    // _log_ref->log("revertWord() -- value = " + to_string(value));

    // преобразуем целое 2х байтовое к однобайтовому
    // затем получаем доступ к старшему байту
    uint8_t hi = (uint8_t)(value >> 8); // вот тут <--- это чтобы получить старщую часть 2х байтового инта
    // операция побитовый сдвиг в прво на 8 бит == на 1 байт
    uint8_t low = (uint8_t) value; // для получения младшего байта ничего особеннного -- просто преобразуем к байту

    // debug
    // _log_ref->log("revertWord() -- revertWord() -- hi = " + to_string(hi));
    // _log_ref->log("revertWord() -- low = " + to_string(low));


    // делаем обмен значениями стандартной функцией 

    // по порядочку
    // сначала берём старший байт (hi) -- кладём его в value -- он ложится сначала в младшую часть, нам такого не надо
    // -- сдвигаем его вправо -- теперь он на старшем месте как нам и надо
    // далее добавляем младший байт. Для безопасности делаем это через побитовый ИЛИ ( | ).

    std::swap(hi, low);
    value = (hi << 8) | low; // коструируем новый 2х байтовый инт
    // debug
    // _log_ref->log("revertWord() -- new value = " + to_string(value));
}

// переворот байт в BE / LE  для 4 байтового целого
void Block::revertDoubleWord(uint32_t& value){
    // _log_ref->log("revertDoubleWord()");
    // _log_ref->log("revertDoubleWord() -- value = " + to_string(value));

    // аналогично revertWord --- см выще -- разница тока в размере интов

    uint16_t hi = (uint16_t)(value >> 16);
    uint16_t low = (uint16_t)value;

    // _log_ref->log("revertDoubleWord() -- hi = " + to_string(hi));   
    // _log_ref->log("revertDoubleWord() -- low = " + to_string(low));

    revertWord(hi);
    revertWord(low);
    swap(hi, low);
    value = (hi << 16) | low; 
    // _log_ref->log("revertDoubleWord() -- new value = " + to_string(value));
}

void Block::revertQuadWord(uint64_t& value){
    // _log_ref->log("revertQuadWord()");
    // _log_ref->log("revertQuadWord() -- value = " + to_string(value));

    // аналогично revertWord --- см выще -- разница тока в размере интов

    uint32_t hi = (uint32_t)(value >> 32);
    uint32_t low = (uint32_t)value;

    // _log_ref->log("revertQuadWord() -- hi = " + to_string(hi));   
    // _log_ref->log("revertQuadWord() -- low = " + to_string(low));

    revertDoubleWord(hi);
    revertDoubleWord(low);
    swap(hi, low);
    value = (hi << 32) | low; 
    // _log_ref->log("revertDoubleWord() -- new value = " + to_string(value));
}

// захексить buffer в поток. ТОесть сделать так чтобы в stream каждый элемент буфера
// передался как HEX значение.
static void hexify(std::stringstream& stream, uint8_t* buffer, uint64_t count){
    stream << std::hex;   
    for (int32_t i = 0; i < sizeof(buffer); i++){
        // если число меньше hex-десятки (или десятичной 16) 
        // то добавляем перед ним нулик
        if (buffer[i] < 16){
            stream << 0;
        }
        stream << (uint16_t)buffer[i] << " ";
    }
    stream << std::dec;
}

// получить строковое hex представление блока
std::string Block::toHexString(){
    std::stringstream ss;   
    hexify(ss, (uint8_t*)&_type, sizeof(_type));
    hexify(ss, (uint8_t*)&_length, sizeof(_length));
    return ss.str();
}

/* HeaderBlock */

// конструктор 
HeaderBlock::HeaderBlock(Log* log_ref, BinUtils* bin_utils_ref): Block(log_ref, bin_utils_ref), _order_magic(0){}

// чтение из потока полей блока с учётом флага BE
void HeaderBlock::parse(std::ifstream& stream, bool big_end){
    // вызов parse предка
    Block::parse(stream, big_end); 

    // читаем из потока данные для заголовочного блока 
    stream.read((char*)&_order_magic, sizeof(_order_magic)); // 4
    stream.read((char*)&_major_version, sizeof(_major_version)); // 2
    stream.read((char*)&_minor_version, sizeof(_minor_version)); // 2
    stream.read((char*)&_section_length, sizeof(_section_length)); // 8

    // если есть признак BigEndian в файле с блоками
    // то нам нужно переворачивать все прочитанные числа 
    // согласно Big Endian порядку байт для целочисленных 
    // переменных
    if (_order_magic == BOM_BE){
        revertDoubleWord(_length);
        // fix: 
        revertWord(_major_version);
        revertWord(_minor_version);
        revertQuadWord(_section_length);
    }

    parseOptions(stream, big_end);
//    _options = _opt_parser->parse(stream);

    uint32_t empty = 0;
    stream.read((char*)&empty, sizeof(empty));
    if (_order_magic == BOM_BE){
        revertDoubleWord(empty);
    }

    if (empty != length()){
        if (_log_ref) {
            _log_ref->log("HeaderBlock: error");
        }
    }
}

uint32_t& HeaderBlock::order_magic(){   
    return _order_magic;
}

uint16_t& HeaderBlock::minor_version(){
    return _major_version;
}

uint16_t& HeaderBlock::major_version(){
    return _minor_version;
}

uint64_t& HeaderBlock::section_length(){
    return _section_length;
}

bool HeaderBlock::extractOption(uint16_t code, uint16_t len, std::ifstream& stream, bool big_end) {
    switch(code){
        case 2:
            // todo: 
            break;
        case 3:
            // todo: 
            break;
        case 4:
            // todo: 
            break;
        default:
            return Block::extractOption(code, len, stream, big_end);
    }
}

void HeaderBlock::output(std::ostream& stream){
    // fix:
    Block::output(stream);
    stream << "order_magic = " << order_magic() << std::hex << "[" << order_magic() << "]" << std::dec << endl
        << "minor_version = "  << minor_version() << endl
        << "major_version = "  << major_version() << endl
        << "section_length = " << section_length() << endl;   
}

/* InterfaceDescriptionBlock */

InterfaceDescriptionBlock::InterfaceDescriptionBlock(Log* log_ref, BinUtils* bin_utils_ref)
    : Block(log_ref, bin_utils_ref), _link_type(0), 
    _reserved(0), _snap_len(0)
{}

bool InterfaceDescriptionBlock::extractOption(uint16_t code, uint16_t len, std::ifstream& stream, bool big_end) {
    uint8_t* buffer = nullptr;
    switch(code){
        case 9: // if_tsresol
            break;
        case 6: // 
            break;
        default:
            return Block::extractOption(code, len, stream, big_end);
    }
    _options.push_back(new Option(code, len));
    uint32_t padding_len = len + sizeof(uint32_t) + 3; // ахтунг! надо срочно научится считать padding(выравнивание)
    buffer = new uint8_t[padding_len];
    stream.read((char*)buffer, padding_len); 
    delete[] buffer;
    return true;
}

void InterfaceDescriptionBlock::parse(std::ifstream& stream, bool big_end) {
    Block::parse(stream, big_end);

    stream.read((char*)&_link_type, sizeof(_link_type)); // 2
    stream.read((char*)&_reserved,  sizeof(_reserved)); // 2
    stream.read((char*)&_snap_len,  sizeof(_snap_len)); // 4 
    
    parseOptions(stream, big_end);

    // fix: вместо буфера могут быть какие то данные 
    // возможно опции
    /// 32 - 8 = 24 - 8 = 16
    // uint8_t buffer[12];
    // stream.read((char*)buffer, sizeof(buffer));

    uint32_t last_len = 0;
    stream.read((char*)&last_len, sizeof(last_len));
    if (big_end){
        revertDoubleWord(last_len);
    }

    _log_ref->log("InterfaceDescriptionBlock::parse() -- _length = " + to_string(_length));
    _log_ref->log("InterfaceDescriptionBlock::parse() -- _length = " + to_string(last_len));

    if (last_len != length()){
        if (_log_ref){
            _log_ref->log("InterfaceDescriptionBlock: error");
        }
    }   
}

std::string InterfaceDescriptionBlock::optCodeToStr(uint16_t code){
    switch(code){
        case 9:// if_tsresol:
            return "if_tsresol";
        default:
            return Block::optCodeToStr(code);
    }

}

void InterfaceDescriptionBlock::output(std::ostream& stream) {
    Block::output(stream);
    stream << "link_type = " << linkType() << endl;
}

uint16_t& InterfaceDescriptionBlock::linkType(){
    return _link_type;
}

/* EnhancedPacketBlock */

EnhancedPacketBlock::EnhancedPacketBlock(Log* log_ref, BinUtils* bin_utils_ref): 
    Block(log_ref,  bin_utils_ref),
    _interface_id(0), 
    _timestamp_hi(0),
    _timestamp_low(0),
    _captured_packet_len(0),
    _original_packet_len(0),
    _packet_data(nullptr)
{}

bool EnhancedPacketBlock::extractOption(uint16_t code, uint16_t len, std::ifstream& stream, bool big_end) {
    _log_ref->log("EnhancedPackedBlock::extractOption");
    switch(code){
        default:    
            return Block::extractOption(code, len, stream, big_end);
    }
    return true;
}

uint32_t& EnhancedPacketBlock::interface_id(){
    return _interface_id;
}

uint32_t& EnhancedPacketBlock::timestamp_hi(){
    return _timestamp_hi;
}   

uint32_t& EnhancedPacketBlock::timestamp_low(){
    return _timestamp_low;
}

uint32_t& EnhancedPacketBlock::captured_packet_len(){
    return _captured_packet_len;
}

uint32_t& EnhancedPacketBlock::original_packet_len(){
    return _original_packet_len;
}


void EnhancedPacketBlock::parse(std::ifstream& stream, bool big_end) {
    Block::parse(stream, big_end);

    stream.read((char*)&_interface_id, sizeof(_interface_id)); // 4
    stream.read((char*)&_timestamp_hi, sizeof(_timestamp_hi)); // 4
    stream.read((char*)&_timestamp_low, sizeof(_timestamp_low)); // 4
    stream.read((char*)&_captured_packet_len, sizeof(_captured_packet_len)); // 4
    stream.read((char*)&_original_packet_len, sizeof(_original_packet_len)); // 4

    _log_ref->log("captured_packet_len = " + std::to_string(_captured_packet_len));
    _log_ref->log("original_packet_len = " + std::to_string(_original_packet_len));

    _packet_data = new uint8_t[_captured_packet_len];

    // выделили буффер -- массив размера пакета трафика - минус мета данные блока
    uint8_t* buffer = new uint8_t[_captured_packet_len];
    stream.read((char*)buffer, _captured_packet_len);

    // скопировали в данные пакета
    for (uint32_t i = 0; i < _captured_packet_len; i++){
        _packet_data[i] = buffer[i];
    }

    uint32_t padding_len = _captured_packet_len % 2; // magic... 
    uint8_t* padding = new uint8_t[padding_len];
    stream.read((char*)padding, padding_len);

    // удалили буффер после использования
    delete[] padding;
    delete[] buffer;

    // (4 - len % 4) % 4

    parseOptions(stream, big_end);


    // читаем в конце пакета длину и сравниваем с прочитанной ранее
    uint32_t last_len = 0;
    stream.read((char*)&last_len, sizeof(last_len));
    if(big_end){
        revertDoubleWord(last_len);
    }

    // если длины не совпадают - значит прочитали что то не правильно
    if (last_len != length()){
        // если задали логгер то пишем в логгер иначе в только в консоль
        if (_log_ref){
            _log_ref->log("EnhancedPacketBlock: error");
        }
    }
}

EnhancedPacketBlock::~EnhancedPacketBlock(){
    delete[] _packet_data; // чистим за собой память
}

void EnhancedPacketBlock::output(std::ostream& stream){
    // fix: 
    Block::output(stream);
    stream << "interface_id = " << interface_id() << endl
        << "timestamp_hi = " << timestamp_hi() << endl
        << "timestamp_low = " << timestamp_low() << endl
        << "captured_packet_len = " << captured_packet_len() << endl
        << "original_packet_len = " << original_packet_len() << endl;

    // hex-печать секции данных
    int32_t len = _captured_packet_len;
    stream << hex;
    for (int32_t i = 0; i < len; i++){
        if (uint32_t(_packet_data[i]) < 16){
            stream << 0;
        }
        stream << uint32_t(_packet_data[i]) << " ";
        if ( ( (i + 1) % 16) == 0){
            stream << endl;
        }       
    }
    stream << dec << endl;
}

/* SimplePacketBlock */

SimplePacketBlock::SimplePacketBlock(Log* log_ref, BinUtils* bin_utils_ref) : 
    Block(log_ref, bin_utils_ref)
{}

void SimplePacketBlock::parse(std::ifstream& stream, bool big_end){
    Block::parse(stream, big_end);

    uint8_t* buffer = new uint8_t[length() - 12 ];
    stream.read((char*)buffer, length() - 12);
    delete[] buffer;
    uint32_t last_len = 0;

    // work in progress

    stream.read((char*)&last_len, sizeof(last_len));
    if (last_len != length()){
        if (_log_ref){
            _log_ref->log("SimplePacketBlock: error");
        }
    }
}