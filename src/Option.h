#pragma once 

#include <cstdint>
#include <string>
#include <vector>

#include "OptionCodes.h"

// Опция блока --
class Option{
private:
    uint16_t _code; // код опции -- для определения типа опции 
    uint16_t _len; // длина опции 
    std::vector<uint8_t> _value; // байты в которых записано значение опции
public:
    Option(uint16_t code, uint16_t len);
    uint16_t code(); // код 
    uint16_t len(); // размер 
    void addValue(uint8_t byte);
    std::vector<uint8_t>& value();
    std::string toString();
};

using OptionsVec = std::vector<Option*>;