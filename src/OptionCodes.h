#pragma once




// код опции -- по нему определяется что за
// опция перед нами
enum OptionCode{
    opt_endofopt = 0,
    opt_comment = 1,
    opt_custom_01 = 2988,//  
    opt_custom_02 = 2989, //
    opt_custom_03 = 19372, //
    opt_custom_04 = 19373, //
    // hedaer block option's
    opt_shb_hardware = 2,
    opt_shb_os       = 3,
    opt_shv_userapp  = 4
};