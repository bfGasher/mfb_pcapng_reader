#include "App.h"

#include <iostream>

App::App(): 
    _reader(nullptr), 
    _bin_utils(nullptr), 
    _log(new Log("app"))
{}

// static впереди делает ф-ию isFileExists видимой только ВНУТРИ ТЕКУЩЕГО СПП ФАЙЛА
// тоесть ни где в другом файле не известно о данной функции.
static bool isFileExists(const char* file_name){
    std::ifstream fake_taxi_stream(file_name);
    bool is_exist = false;
    if (fake_taxi_stream.is_open()){
        is_exist = true;
    }
    fake_taxi_stream.close();
    return is_exist; 
}

// старт программы
// argc - количество аргументов командной строки
// argv - массив строк аргументов командной строки
void App::start(int argc, char** argv){
    // выставить  режим записи лога в печать на консоль
    _log->setPrintToConsole(true);
    if (argc > 1){       
        if (!isFileExists(argv[1])){
            _log->log("File not exist's : " + std::string(argv[1]));
        } else {
            _log->log("Filename: " + std::string(argv[1]));
            _bin_utils = new BinUtils(_log);
            _reader = new Reader(_log, _bin_utils, argv[1]);           

            // fix: сделать проверку на существование файла, для безопасности.           

            // debug: отладка заголовочного блока
            /*for (int i = 0 ; i < 2; i++){
                Block* block = _reader->next();
                if (block){
                    block->output(std::cout);
                    block->outputOptions(std::cout);
                    delete block;
                }    
            }*/
         
            // цикл чтения блоков открытого ранее файла pcapng
            Block* block = nullptr;
            while (true){
                // пока можем получать блоки из 
                // открытого читателем файла -- 
                // хватаем и парсим блоки -- 
                // затем печатаем то что они содержат если
                // это возможно
                block = _reader->next();
                if (block){
                    block->output(std::cout);
                    block->outputOptions(std::cout);
                    // _log->log("hex of block");
                    // _log->log(block->toHexString());
                    delete block; // fix: можно добавлять в какой нить список потом    
                }else{
                    break;
                }            
            }        
        }
    }else{
        // подсказка, как пользоваться программой выводимая на 
        _log->log("usage [app-name] [filename.pcapng]");
    }    
}

App::~App(){
    // сохраняем записанный лог в файл
    _log->save(); 
    // чистим память
    delete _reader;  
    delete _bin_utils;
    delete _log;
}
