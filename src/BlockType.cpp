#include "BlockType.h"

std::string blockTypeToStr(uint32_t type){
    switch(type){
        case BLOCK_TYPE_SECTION_HEADER_BLOCK:
            return "Section header";        
        case BLOCK_TYPE_INTERFACE_DESCRIPTION_BLOCK:
            return "Interface description"; 
        case BLOCK_TYPE_ENHANCED_PACKET_BLOCK:
            return "Enhanced packet"; 
        case BLOCK_TYPE_SIMPLE_PACKET_BLOCK:
            return ""; 
        case BLOCK_TYPE_NAME_RESOLUTION_BLOCK:
            return ""; 
        case BLOCK_TYPE_INTERFACE_STATISTICS_BLOCK:
            return ""; 
        case BLOCK_TYPE_CUSTOM_BLOCK_COPY_ALLOWED:
            return ""; 
        case BLOCK_TYPE_CUSTOM_BLOCK_COPY_FORBIDDEN:
            return "Custom block copy forbiden"; 
        default:
            return "unknown";
    }
}