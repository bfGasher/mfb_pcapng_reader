#include "Reader.h"

#include "Block.h"
#include "BlockType.h"

Reader::Reader(Log* log_ref, BinUtils* bin_utils, const std::string& file_name)
    : _stream(file_name, std::ios::in | std::ios::binary), 
    // создаём фабрику блоков для ридера
    _factory(new BlockFactory(log_ref)),
    _log_ref(log_ref),   
    _bin_utils_ref(bin_utils),
    _header_readed(false), 
    _big_endian(false)
{}   

Reader::~Reader(){
    // чистим за собой всякое
    _stream.close(); // закрываем поток
    delete _factory; // удаление фабрик
    // удалёем в деструкторе класса App
    _log_ref = nullptr; 
    _bin_utils_ref = nullptr;
}  

// ф-ия переворачивания 
// fix: применить данную функцию для переворота байтов
// требуется порефакторить данную функцию ибо она не 
// безопасна для промышленного использования.
void reverseEndian(uint32_t* var) {
    uint32_t tmp;
    int j = 3;
    for(int i = 0; i < 4; i++) {
        // Swap the bits around
        *(((uint8_t*) &tmp) + j) = *(((uint8_t*) var)+i);
        j--;
    }
    *var = tmp;
}

// Прочитать следующий блок из файла
Block* Reader::next(){
    if ( ! _header_readed ){
       return readHeader();
    } else {
        _log_ref->log("---------- block reading -------------");
        if (_stream.eof()){
            _log_ref->log("---------- block not readed -- eof");
            return nullptr;            
        } else {
            uint32_t type = 0;
            uint32_t len  = 0; // todo: 
            uint32_t bom  = 0; // todo:            
           
            // читаем тип блока
            _stream.read( (char*)&type, sizeof(type) );
            /// если BE то переварачиваем его
            if (_big_endian){
                reverseEndian(&type);
            }

            if (_stream.eof()){
                _log_ref->log("---------- block not readed -- eof");
                return nullptr;           
            }else{
                Block* block = nullptr;
                int64_t pos = _stream.tellg(); // берём текущую позицию
                // колдунство. прочитали тип и шагнули назад на 4 байта     
                _stream.seekg(pos - sizeof(type), std::ios::beg); // шагаем взад
                block = _factory->make(type, _bin_utils_ref); // создаём блок по прочитанному типу ранее
                block->parse(_stream, _big_endian); // читаем данные из потока блоком
                _log_ref->log("---------- block readed -------- ");

                return block; // возвращаем блок
            }
        }       
    }
}

// проверка -- можем читать // работает странно потому может быть не использованно....
bool Reader::canRead() const{
    return !_stream.eof();
}

// чтение заголовочного блока из файла. 
Block* Reader::readHeader() {
    _log_ref->log("---------- header reading -------------");
    HeaderBlock* block = (HeaderBlock*)_factory->make(BLOCK_TYPE_SECTION_HEADER_BLOCK, _bin_utils_ref); // строим заголовочнй блок из фабрики
    block->parse(_stream, _big_endian); // читаем из потока
    _header_readed = true; // говорим ридеру что заголовочный блок прочитан

    // если в документе записаны числа в Big Endian порядке байт
    // то выставляем соответственный флаг
    _big_endian = false;
    if (block->order_magic() == BOM_BE){
        _big_endian = true;
    }
    _log_ref->log("---------- header readed -------------");
    return block;
}

