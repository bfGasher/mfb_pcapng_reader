#include "Option.h"

/*
    opt_endofopt = 0,
    opt_comment = 1,
    opt_custom_01 = 2988,//  
    opt_custom_02 = 2989, //
    opt_custom_03 = 19372, //
    opt_custom_04 = 19373, //
    // hedaer block option's
    opt_shb_hardware = 2,
    opt_shb_os       = 3,
    opt_shv_userapp  = 4
*/

static std::string OptCodeToStr(uint16_t code){
    switch(code){
        case opt_comment:
            return "opt_commnet";
        default:
            return "unknown yet option code";
    }
}

Option::Option(uint16_t code, uint16_t len):
    _code(code), _len(len), _value()
{}

uint16_t Option::code(){
    return _code;
}

// получить доступ к переменке _len  
uint16_t Option::len(){
    return _len;
}

// 
std::vector<uint8_t>& Option::value(){
    return _value;
}

void Option::addValue(uint8_t byte){
    _value.push_back(byte);
}

std::string Option::toString(){
    return "[code: " + OptCodeToStr(_code) + ", len: " + std::to_string(_len) + "]";
}