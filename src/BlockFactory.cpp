#include "BlockFactory.h"

#include "BlockType.h"

#include <iostream>

using std::cout;
using std::endl;

BlockFactory::BlockFactory(Log* log_ref):
    _log_ref(log_ref)
{}

// создать блок-объект по типу
Block* BlockFactory::make(uint32_t type, BinUtils* bin_utils_ref){        
    switch(type){ // в зависимости от типа который мы приняли...
        // строим один из следующих блоков

        // заголовочный блок 
        case BLOCK_TYPE_SECTION_HEADER_BLOCK:            
            _log_ref->log("Header block created");

            // в блоки передаём логгер чтобы не создавать его везжде и не делать
            // глобальным. Глобалки и Синглтоны --- злооо
            return new HeaderBlock(_log_ref, bin_utils_ref); 
        // енхансед блок
        case BLOCK_TYPE_ENHANCED_PACKET_BLOCK:
            _log_ref->log("EnhancedPacketBlock block created");
            return new EnhancedPacketBlock(_log_ref, bin_utils_ref);       
        // интерфейсный блок
        case BLOCK_TYPE_INTERFACE_DESCRIPTION_BLOCK:                      
            _log_ref->log("Interface description block");
            return new InterfaceDescriptionBlock(_log_ref, bin_utils_ref);

        // если попали сюда, значит что мы пока не знаем что это за блок
        // либо мы пока не добрались до блока читателем либо 
        // пока не ввели поддержку данного блока в программу (такое тоже может быть)
        default:                       
            _log_ref->log("unknown block type");
            return new Block(_log_ref, bin_utils_ref);
    }        
}