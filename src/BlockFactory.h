#pragma once 

#include <cstdint>

#include "Block.h"
#include "Log.h"

// Фабрика блоков

class BlockFactory {
private:
    Log* _log_ref; // ссылка на логер
public:
    BlockFactory(Log* log_ref);
    Block* make(uint32_t type, BinUtils* bin_utils_ref);
};