#pragma once 

#include <string>
#include <iostream>
#include <fstream>
#include <cstdint>

#include "Log.h"
#include "BlockFactory.h"

// читатель файлов *.pcapng
class Reader{
private:
    std::ifstream _stream; // файловый поток
    BlockFactory* _factory; // фабрика блоков 
    Log* _log_ref; // ссылка на логгер 
    BinUtils* _bin_utils_ref; // ссылка на утилитный класс работы с байтами
    bool _header_readed; // прочитан ли был заголовочный блок 
    bool _big_endian; // биг эндиан расположение байт для целых чисел
public: 
    Reader(Log* log_ref, BinUtils* bin_utils, const std::string& file_name);   
    virtual ~Reader();

    bool canRead()const; // могу читать дальше?
    Block* next(); // читатем след блок файла
    Block* readHeader(); // читаем заголовочный блок файла
};