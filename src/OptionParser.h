#pragma once 

#include <cstdint>
#include <iostream>
#include <fstream>
#include <vector>

#include "Log.h"
#include "BinUtils.h"
#include "Option.h"

class OptionParser{
private:
    Log* _log_ref;
    BinUtils* _bin_utils_ref;
public:
    OptionParser(Log* log_ref, BinUtils* bin_utils_ref);
    OptionsVec parse(std::ifstream& stream);
};
