#pragma once 

#include "Reader.h"
#include "BinUtils.h"
#include "Log.h"

// Главный класс приложения
class App{
private:
    Reader*       _reader;     // указатель на читатель файла
    BinUtils*     _bin_utils;  // класс утилита для работы с байтами
    Log*          _log;        // указатель на логгер приложения
public:
    App(); // конструктор 
    ~App(); // деструктор
    void start(int argc, char** argv); // старт программы
};